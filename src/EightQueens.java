
public class EightQueens {

	public Gameboard board;
	
	public EightQueens() {
		// TODO Auto-generated constructor stub
		board = new Gameboard(8);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		EightQueens trial = new EightQueens();
		
		System.out.println("after initialization");
		trial.board.printBoard();
		
		trial.board.setField(1, 1, 0);
		trial.board.setField(1, 5, 0);
		trial.board.setField(3, 3, 0);
		
		System.out.println("after setting three queens");
		trial.board.printBoard();
		
		System.out.println("check the rows");
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
		
				System.out.print("\t"+trial.board.checkRow(j, i));
			}
			System.out.println();
		}
		
		System.out.println("check the columns");
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
		
				System.out.print("\t"+trial.board.checkColumn(j, i));
			}
			System.out.println();
		}
		
		System.out.println("check the diagonals");
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
		
				System.out.print("\t"+trial.board.checkDiagonal(j, i));
			}
			System.out.println();
		}
		
		System.out.println("check the antidiagonals");
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
		
				System.out.print("\t"+trial.board.checkAntiDiagonal(j, i));
			}
			System.out.println();
		}
		System.out.println("check ALL");
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
		
				System.out.print("\t"+trial.board.checkAll(j, i));
			}
			System.out.println();
		}
		
		EightQueens trial2 = new EightQueens();
		
		System.out.println("after initialization");
		trial2.board.printBoard();
		
		
		trial2.board.fillBoard8QueensRecursiv(8, 0, 0);
		}

}
