
public class Gameboard {

	private int[][] gameboard;	
	private int length;
	
	public Gameboard(int length) {
		// TODO Auto-generated constructor stub
		this.length = length;
		gameboard = new int[length][length];
		this.initBoard();
	}
	
	private void initBoard(){
		for (int i = 0; i < length; i++){
			for (int j = 0; j < length; j++){
				gameboard[j][i]=-1;
			}
		}
	}
	
	public boolean fillBoard8QueensRecursiv(int numberOfQueens, int rowToSet, int columnToSet){
//		System.out.println("curent row, column: "+rowToSet+", "+columnToSet);
		if(columnToSet>=8){
//			System.out.println("uppps");
			return false;
		}
		//dann schau ob sich die positionen aus dem argument aufs board setzen lasse
		boolean possibleToSetTheQueen = this.checkAll(columnToSet,rowToSet);
		if (possibleToSetTheQueen) {
			//falls ja: setze die dame (0) auf die position
			this.setField(columnToSet, rowToSet, 0);
//			System.out.println("####### setted to queen "+rowToSet+", "+columnToSet);
			// falls hier numberOfQueens 1 war (sozusagen wurde die letzte gesetzt!): return true. L�sung wurde gefunden
			if (numberOfQueens == 1) {
				this.printBoard();
				return true;
			}else{
				//ansonsten weitersuchen angesagt (n�chste row! juhu) - Recursion
				boolean possibleToSetTheNEXTQueens = this.fillBoard8QueensRecursiv(numberOfQueens-1, rowToSet+1, 0);
				if(possibleToSetTheNEXTQueens){
					return true;
				}
				else{
					//lieffert die rekursion nen false (eingeschlagener weg war falsch): l�sche die dame von der aktuellen position
					this.setField(columnToSet, rowToSet, -1);
//					System.out.println("### deleted the queen at "+rowToSet+", "+columnToSet);
					//verlasse die recursivit�t an dieser stelle
				}
			}
		}
		//checke den n�chsten nachbarn
		return this.fillBoard8QueensRecursiv(numberOfQueens, rowToSet, columnToSet+1);
	}
	
	public int[][] getCopyOfBoard(){
		int[][] copy = new int[length][length];		
		for (int i = 0; i < length; i++){
			for (int j = 0; j < length; j++){
				copy[j][i]=gameboard[j][i];
			}
		}
		return copy;
	}
	
	public int[][] getAllQueensAtBoard(){
		int[][] queensarray = new int[8][2];
		int queennumber = 0;
		for (int i = 0; i < length; i++){
			for (int j = 0; j < length; j++){
				if(gameboard[j][i]==0){
					queensarray[queennumber][0]=j;
					queensarray[queennumber][1]=i;
					queennumber++;
				}
			}
		}
		return queensarray;
	}
	
	public boolean checkAll(int column, int row) {
		// TODO Auto-generated method stub
		return this.checkColumn(column, row) && this.checkRow(column, row) &&
				this.checkDiagonal(column, row) && this.checkAntiDiagonal(column, row);
	}
	
	public boolean checkColumn(int column, int row){
		for (int i = 0; i<length; i++){
			if(0==gameboard[column][i]){
				return false;
			}
		}
		return true;
	}
	
	public boolean checkRow(int column, int row){
		for (int i = 0; i<length; i++){
			if(0==gameboard[i][row]){
				return false;
			}
		}
		return true;
	}
	
	public boolean checkDiagonal(int column, int row){
		try {
			for (int i = 0; i<length; i++){
				if(0==gameboard[column+i][row-i]){
					return false;
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		try {
			for (int i = 0; i<length; i++){
				if(0==gameboard[column-i][row+i]){
					return false;
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		return true;
	}
	public boolean checkAntiDiagonal(int column, int row){
		try {
			for (int i = 0; i<length; i++){
				if(0==gameboard[column-i][row-i]){
					return false;
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		try {
			for (int i = 0; i<length; i++){
				if(0==gameboard[column+i][row+i]){
					return false;
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		return true;
	}
	
	public void printBoard(){
		for (int i = 0; i < length; i++){
			for (int j = 0; j < length; j++){
				System.out.print("  "+gameboard[j][i]);
			}
			System.out.println();
		}
	}

	public void setField(int column, int row, int value){
		gameboard[column][row]=value;
	}
	
	public int getField(int column, int row){
		return gameboard[column][row];
	}	
}
